class lysntp {
  case $facts['os']['family'] {
    'RedHat': {
      # Disable ntpd if installed
      #  - on el7, ipa-client depends on ntp, so we cannot uninstall it
      #  - on later systems, ntpd is not installed by default, so we cannot use a service resource
      exec { '/bin/systemctl disable ntpd.service':
        onlyif => '/bin/systemctl is-enabled ntpd',
      }
      exec { '/bin/systemctl stop ntpd.service':
        onlyif => '/bin/systemctl is-active ntpd',
      }

      ensure_packages( ['chrony'], {
        before => [
          File['/etc/chrony.conf'],
          Service['chronyd'],
        ],
      })
      file { '/etc/chrony.conf':
        owner   => root,
        group   => root,
        mode    => '0644',
        content => epp('lysntp/chrony.conf.epp', {
          driftfile => '/var/lib/chrony/drift',
          keyfile   => '/etc/chrony.keys',
          logdir    => '/var/log/chrony',
        }),
      }
      ~> service { 'chronyd':
        ensure => running,
        enable => true,
      }
    }
    'Debian': {
      ensure_packages( ['ntp'], { ensure => absent } )

      ensure_packages( ['chrony'], {
        before => [
          File['/etc/chrony.conf'],
          Service['chrony'],
        ],
      })
      file { '/etc/chrony.conf':
        owner   => root,
        group   => root,
        mode    => '0644',
        content => epp('lysntp/chrony.conf.epp', {
          driftfile => '/var/lib/chrony/drift',
          keyfile   => '/etc/chrony.keys',
          logdir    => '/var/log/chrony',
        }),
      }
      ~> service { 'chrony':
        ensure => running,
        enable => true,
      }
    }
    'FreeBSD': {
      service { 'ntpd':
        ensure => stopped,
        enable => false,
      }
      ensure_packages( ['chrony'], {
        before => [
          File['/usr/local/etc/chrony.conf'],
          Service['chronyd'],
        ],
      })
      file { '/usr/local/etc/chrony.conf':
        owner   => root,
        group   => wheel,
        mode    => '0644',
        content => epp('lysntp/chrony.conf.epp', {
          driftfile => '/var/db/chrony/drift',
          keyfile   => '/usr/local/etc/chrony.keys',
          logdir    => '/var/log/chrony',
        }),
      }
      ~> service { 'chronyd':
        ensure => running,
        enable => true,
      }
    }
    default: {
      fail("Operating system ${facts['os']['family']} currently not supported")
    }
  }
}
